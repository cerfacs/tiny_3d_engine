tiny\_3d\_engine package
========================

.. automodule:: tiny_3d_engine
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

tiny\_3d\_engine.cli module
---------------------------

.. automodule:: tiny_3d_engine.cli
   :members:
   :undoc-members:
   :show-inheritance:

tiny\_3d\_engine.color module
-----------------------------

.. automodule:: tiny_3d_engine.color
   :members:
   :undoc-members:
   :show-inheritance:

tiny\_3d\_engine.engine module
------------------------------

.. automodule:: tiny_3d_engine.engine
   :members:
   :undoc-members:
   :show-inheritance:

tiny\_3d\_engine.geo\_loader module
-----------------------------------

.. automodule:: tiny_3d_engine.geo_loader
   :members:
   :undoc-members:
   :show-inheritance:

tiny\_3d\_engine.part3d module
------------------------------

.. automodule:: tiny_3d_engine.part3d
   :members:
   :undoc-members:
   :show-inheritance:

tiny\_3d\_engine.ply\_loader module
-----------------------------------

.. automodule:: tiny_3d_engine.ply_loader
   :members:
   :undoc-members:
   :show-inheritance:

tiny\_3d\_engine.scene3d module
-------------------------------

.. automodule:: tiny_3d_engine.scene3d
   :members:
   :undoc-members:
   :show-inheritance:

tiny\_3d\_engine.screen module
------------------------------

.. automodule:: tiny_3d_engine.screen
   :members:
   :undoc-members:
   :show-inheritance:

tiny\_3d\_engine.viewfield module
---------------------------------

.. automodule:: tiny_3d_engine.viewfield
   :members:
   :undoc-members:
   :show-inheritance:

