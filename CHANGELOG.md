# Changelog

All notable changes to this project will be documented in this file.
The format is based on Keep a Changelog,
and this project adheres to Semantic Versioning.

## [0.2.1] 2022 / 02 / 22

### Added

 - setup.cfg

### Changed

 - setup.py, cli to add versionning

## [0.2] 2020 / 05 / 12

### Added
 - Benchmarks
 - CLI
 - Shadings
 - support .ply files
 - easier view controls

### Changed
 - load_geo_as_scene deprecated goes to load_file_as_scene

### Fixed
 - axes are no more duplicated

### Deprecated
 [ - ] 

## [0.1] 2020 / 04 / 15

### Added
 - initial working version

### Changed
 [ - ]

### Fixed
 [ - ] 

### Deprecated
 [ - ] 


